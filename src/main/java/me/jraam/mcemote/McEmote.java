package me.jraam.mcemote;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Nathaniel (jraamus/jraam)
 */
public class McEmote extends JavaPlugin implements Listener{
    @Override
    public void onEnable() {
        getLogger().info("Checking config...");
        getConfig().options().copyDefaults(true);
        saveConfig();
        if(getConfig().contains(BIG_EMOTE_CFG_KEY)){
            enable_big_chat_emotes = getConfig().getBoolean(BIG_EMOTE_CFG_KEY, true);
            getLogger().info("Config loaded.");
        }
        else{
            getConfig().set(BIG_EMOTE_CFG_KEY, true);
            enable_big_chat_emotes = true;
            saveConfig();
            getLogger().info("Initialized config.");
        }
        getServer().getPluginManager().registerEvents(this, this);
        getLogger().info("Checking for emotes...");
        File emotes = new File("EMOTES.txt");
        boolean success = false;
        emote_dictionary = new ArrayList<>();
        try {
            success = !(emotes.createNewFile());
        } catch (IOException ex) {
            getLogger().info("Failed to load emotes: IO error");
        }
        if(!success){
            getLogger().info("No emotes found.");
        }
        else{
            int ln_num = 1;
            try {
                Scanner s = new Scanner(emotes);
                while(s.hasNextLine()){
                    String currentline = s.nextLine();
                    String[] entry = currentline.split(",");
                    String new_name = entry[0];
                    int new_unicode = Integer.parseInt(entry[1], 16);
                    emote_dictionary.add(new Emote(new_name, new_unicode));
                    ln_num ++;
                }
            } catch (Exception ex) {
                getLogger().log(Level.INFO, "Error reading emotes at line: {0}", ln_num);
            }
            int num_entries = emote_dictionary.size();
            getLogger().log(Level.INFO, "{0} emotes found.", num_entries);
        }
    }

    @Override
    public void onDisable() {
        //Disable code
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
	if (cmd.getName().equalsIgnoreCase("listemotes")) {
            int emotepages = emote_dictionary.size() / 15 + 1;
            if(args.length < 1){
                if(emotepages == 1){
                    sender.sendMessage("page 1/1");
                    for (Emote entry : emote_dictionary) {
                        sender.sendMessage(entry.getName() + ":" + entry.getEmoteChar());
                    }
                }
                else{
                    sender.sendMessage("Please type your command with a page number: \n /listemotes [page number]\n there are " + emotepages + " pages.");
                }
            }
            else{
                try{
                    int pagenumber = Integer.parseInt(args[0]);
                    if(pagenumber >= emotepages){
                        pagenumber = emotepages;
                        sender.sendMessage("page " + pagenumber + "/" + emotepages);
                        int low = (pagenumber - 1)*15;
                        int high = pagenumber*15;
                        if(high > emote_dictionary.size()){
                            high = emote_dictionary.size();
                        }
                        for(int i = low; i < high; i++){
                            sender.sendMessage(emote_dictionary.get(i).getName() + ":" + emote_dictionary.get(i).getEmoteChar());
                        }
                    }
                    else if(pagenumber <= 0){
                        sender.sendMessage("Invalid command");
                    }
                    else{
                        sender.sendMessage("page " + pagenumber + "/" + emotepages);
                        int low = (pagenumber - 1)*15;
                        int high = pagenumber*15;
                        for(int i = low; i < high; i++){
                            sender.sendMessage(emote_dictionary.get(i).getName() + ":" + emote_dictionary.get(i).getEmoteChar());
                        }
                    }
                } catch(Exception ex){
                    sender.sendMessage("Invalid command");
                }
            }
            return true;
	}
        else if(cmd.getName().equalsIgnoreCase("helpemotes")){
            sender.sendMessage("Chat:");
            sender.sendMessage("Type the name of the emote and it will be replaced by the emote when the message is sent.");
            sender.sendMessage("Emote names can be found using the /listemotes [page number] command.");
            sender.sendMessage("If the message text is only one emote name, the message will be converted to a large version of the emote (3x the standard text height).");
            sender.sendMessage("Anvils:");
            sender.sendMessage("Emote names in the item name will be replaced with the emote when the item is renamed on an anvil.");
            sender.sendMessage("Signs:");
            sender.sendMessage("Emote names will be replaced by the emote upon clicking \"Done\" when editing signs.");
            sender.sendMessage("If the a line of text on the sign is only one emote name and is preceded by two empty lines, a large version of the emote will be used.");
            return true;
        }
        else if(cmd.getName().equalsIgnoreCase("largeemotes")){
            if(sender.isOp()){
                if(args.length == 1){
                    if(args[0].equalsIgnoreCase("true") || args[0].equalsIgnoreCase("t") || args[0].equalsIgnoreCase("1")){
                        enable_big_chat_emotes = true;
                        sender.sendMessage("Large emotes have been enabled");
                        return true;
                    }
                    else if(args[0].equalsIgnoreCase("false") || args[0].equalsIgnoreCase("f") || args[0].equalsIgnoreCase("0")){
                        enable_big_chat_emotes = false;
                        sender.sendMessage("Large emotes have been disabled");
                        return true;
                    }
                }
                sender.sendMessage("Invalid argument");
            }
        }
	return false; 
    }
    
    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e){
        String message = e.getMessage();
        message = " " + message + " ";
        boolean replaced = false;
        for (Emote dictionary_entry : emote_dictionary) {
            if(message.equals(dictionary_entry.getName()) && enable_big_chat_emotes){
                message = " \n \n \n" + dictionary_entry.getBigEmoteChar();
                e.setMessage(message);
                return;
            }
        }
        for (Emote dictionary_entry : emote_dictionary) {
            if(message.contains(dictionary_entry.getName())){
                message = message.replaceAll(dictionary_entry.getName(), dictionary_entry.getEmoteChar());
                replaced = true;
            }
        }
        if(replaced){
            String trimmedmessage = message.substring(1, message.length() - 1);
            e.setMessage(trimmedmessage);
        }
    }
    
    @EventHandler
    public void onAnvilUse(PrepareAnvilEvent e){
        if(e.getResult() != null){
            if (e.getResult().hasItemMeta() && !"".equals(e.getInventory().getRenameText())) {
                
                String item_name = " " + e.getInventory().getRenameText() + " ";
                boolean replaced = false;
                for (Emote dictionary_entry : emote_dictionary) {
                    if(item_name.contains(dictionary_entry.getName())){
                        item_name = item_name.replaceAll(dictionary_entry.getName(), dictionary_entry.getEmoteChar());
                        replaced = true;
                    }
                }
                if(replaced){
                    String new_name = item_name.substring(1, item_name.length() - 1);
                    ItemStack renamed_item = e.getResult();
                    ItemMeta new_meta = renamed_item.getItemMeta();
                    new_meta.setDisplayName(new_name);
                    renamed_item.setItemMeta(new_meta);
                    e.setResult(renamed_item);
                }
                
            }
        }
    }
    
    @EventHandler
    public void onSignEdit(SignChangeEvent e){
        String line0 = "";
        String line1 = "";
        String line2 = "";
        String line3 = "";
        if(e.getLine(0) != null){
            line0 = e.getLine(0);
        }
        if(e.getLine(1) != null){
            line1 = e.getLine(1);
        }
        if(e.getLine(2) != null){
            line2 = e.getLine(2);
        }
        if(e.getLine(3) != null){
            line3 = e.getLine(3);
        }
        //Determine special case where there is only one emote in the bottom 
        //two lines of text and no other text on the sign -> big emote
        boolean ln2 = false;
        String testline = " " + line2 + " ";
        String bigemote = "error";
        for (Emote dictionary_entry : emote_dictionary) {
            if(testline.equals(dictionary_entry.getName())){
                ln2 = true;
                bigemote = "&f" + dictionary_entry.getBigEmoteChar() + "&0";
                bigemote = ChatColor.translateAlternateColorCodes​('&', bigemote);
            }
        }
        boolean ln3 = false;
        testline = " " + line3 + " ";
        for (Emote dictionary_entry : emote_dictionary) {
            if(testline.equals(dictionary_entry.getName())){
                ln3 = true;
                bigemote = "&f" + dictionary_entry.getBigEmoteChar() + "&0";
                bigemote = ChatColor.translateAlternateColorCodes​('&', bigemote);
            }
        }
        if(line1.equals("") && line2.equals("") && ln3){
            e.setLine(3, bigemote);
        }
        else if(line0.equals("") && line1.equals("") && ln2){
            e.setLine(2, bigemote);
        }
        else{//replace emotes by lines
            boolean replaced = false;
            String line = " " + line0 + " ";
            for (Emote dictionary_entry : emote_dictionary) {
                if(line.contains(dictionary_entry.getName())){
                    line = line.replaceAll(dictionary_entry.getName(), dictionary_entry.getSignEmoteChar());
                    replaced = true;
                }
            }
            if(replaced){
                String trimmedline = line.substring(1, line.length() - 1);
                trimmedline = ChatColor.translateAlternateColorCodes('&', trimmedline);
                e.setLine(0, trimmedline);
            }
            replaced = false;
            line = " " + line1 + " ";
            for (Emote dictionary_entry : emote_dictionary) {
                if(line.contains(dictionary_entry.getName())){
                    line = line.replaceAll(dictionary_entry.getName(), dictionary_entry.getSignEmoteChar());
                    replaced = true;
                }
            }
            if(replaced){
                String trimmedline = line.substring(1, line.length() - 1);
                trimmedline = ChatColor.translateAlternateColorCodes('&', trimmedline);
                e.setLine(1, trimmedline);
            }
            replaced = false;
            line = " " + line2 + " ";
            for (Emote dictionary_entry : emote_dictionary) {
                if(line.contains(dictionary_entry.getName())){
                    line = line.replaceAll(dictionary_entry.getName(), dictionary_entry.getSignEmoteChar());
                    replaced = true;
                }
            }
            if(replaced){
                String trimmedline = line.substring(1, line.length() - 1);
                trimmedline = ChatColor.translateAlternateColorCodes('&', trimmedline);
                e.setLine(2, trimmedline);
            }
            replaced = false;
            line = " " + line3 + " ";
            for (Emote dictionary_entry : emote_dictionary) {
                if(line.contains(dictionary_entry.getName())){
                    line = line.replaceAll(dictionary_entry.getName(), dictionary_entry.getSignEmoteChar());
                    replaced = true;
                }
            }
            if(replaced){
                String trimmedline = line.substring(1, line.length() - 1);
                trimmedline = ChatColor.translateAlternateColorCodes('&', trimmedline);
                e.setLine(3, trimmedline);
            }
        }
    }
    
    public boolean enable_big_chat_emotes;
    
    static List<Emote> emote_dictionary;
    
    private static final String BIG_EMOTE_CFG_KEY = "BigEmotesEnabled";
}

class Emote{
    /**
     * Holds the string of text and the unicode value that will replace it.
     * @param name The string that is to be replaced by this emote
     * @param value The unicode value that points to this emote
     */
    public Emote(String name, int value){
        em_name = " " + name + " ";
        em_unicode_value = value;
    }
    
    /**
     * The string that is to be replaced by this emote
     */
    private String em_name;
    
    /**
     * The unicode character that points to this emote
     */
    private int em_unicode_value;
    
    /**
     * Returns the name of the emote
     * @return the string that triggers this emote
     */
    public String getName(){
        return em_name;
    }
    
    /**
     * Returns a string containing the emote
     * @return the emote as a string.
     */
    public String getEmoteChar(){
        String replace = new String(Character.toChars(em_unicode_value));
        return " " + replace + " ";
    }
    
    /**
     * Special version of getEmoteChar() for signs that includes information for colouring the emote text white on signs.
     * @return the emote as a string.
     */
    public String getSignEmoteChar(){
        String replace = new String(Character.toChars(em_unicode_value));
        return " &f" + replace + "&0 ";
    }
    
    /**
     * Returns a string containing the emote
     * @return the emote as a string.
     */
    public String getBigEmoteChar(){
        String replace = new String(Character.toChars(em_unicode_value + 1));
        return " " + replace;
    }
}
