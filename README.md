***********************************************************************************************************************************************
INTRODUCTION

This is a spigot plugin for minecraft that handles emotes for minecraft servers.

An EMOTES.txt file is created in the same file as the server .jar. 

On startup, this file is read and a dictionary is created containing the strings that are replaced by the emote and the unicode for the emote.

The players will be expected to have a resource pack that associates images with the unicode characters reckognized by the server.

EmoteWrangler (https://gitlab.com/jraam1/emotewrangler) is a separate program that generates a filled EMOTES.txt file as well as the files needed to produce a minecraft resource pack.

(See ManagingEmotes.m4v https://gitlab.com/jraam1/mcemote/-/blob/master/ManagingEmotes.m4v (apologies for my awkward voice))

***********************************************************************************************************************************************
PUGIN COMMANDS

/listemotes [page number]

Lists the emotes that are known by the server

***********************************************************************************************************************************************
INSTALLATION

1 - Install a Spigot Minecraft server (https://www.spigotmc.org/wiki/spigot-installation/)

2 - Move the .jar file for the MCEmote plugin (https://gitlab.com/jraam1/mcemote/-/blob/master/target/MCEmote-1.2.jar) into the server's plugins folder. If it downloads as `MCEmote-1.2.jar.zip`, remove the `.zip` from the end of the file name so that is is `MCEmote-1.2.jar` (it appears Gitlab has started doing this to get around browsers cautioning against downloading potentially excecutable files).

3 - Move EMOTES.txt that has been created by EmoteWrangler (https://gitlab.com/jraam1/emotewrangler) to the same folder as the spigot server jar

4 - Setup the resource pack with the default.json and /custom folder created by EmoteWrangler (default.json goes under assets/minecraft/font and /custom should be moved to assets/minecraft/textures)

5 - Run the server and client. Ensure that the client has your custom resource pack enabled.

***********************************************************************************************************************************************
VERSIONS

The plugin jar has been tested for 1.16.\*, 1.17.\*, 1.18.1 and 1.19.2

2022-08-23
